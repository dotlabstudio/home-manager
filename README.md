# Home Manager & DevBox Project

This repo is my newbie journey into home manager and nix on Linux and Mac OS for a portable apps version for these systems.
A better solution maybe nix flakes. Found a better solution in devbox.

# Install the operating system on to real hardware or virtual machine

Good Linux distributions besides NixOS for me are:Tuesday, 27 Jun 2023 at 15:25


openSUSE Kalpa

https://get.opensuse.org/microos/ download page

https://en.opensuse.org/Portal:Kalpa for more info


fedora kinoite

https://fedoraproject.org/kinoite/


openSUSE Tumbleweed

https://get.opensuse.org/tumbleweed/


mx linux (mepis)

https://mxlinux.org/

I like these distributions because either they are immutable, have automatic snapshots or you can create a live snapshot system.

# Install Nix 
On MacOS and normal linux distributions like openSUSE Tumbleweed, mx linux and debian, run;

```
curl -L https://nixos.org/nix/install | sh
```

reference: https://github.com/NixOS/nix

or use the rust installer from Determinate Nix Installer :
```
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
```

see https://determinate.systems/posts/determinate-nix-installer

debian needs curl to be installed first with

```
sudo apt install curl
```

on openSUSE Kalpa:

first run

```
sudo transactional-update run mksubvolume /nix
```

then
reboot

change the file permissions on /nix with:

```
sudo chown -R $USER /nix
```

Temporary set selinux to permissive mode until the next reboot

```
sudo setenforce 0
```

or

```
sudo setenforce permissive
```

then run the normal nix installer script
```
curl -L https://nixos.org/nix/install | sh
```

check selinux still enable and enforcing with:

``` 
sestatus
```

or 

```
sudo getenforce
```

on fedora kinoite and ublue (https://ublue.it/):

```
curl -s https://raw.githubusercontent.com/dnkmmr69420/nix-installer-scripts/main/installer-scripts/silverblue-nix-installer.sh | bash
```

check selinux status with sestatus

on almalinux:

```
curl -s https://raw.githubusercontent.com/dnkmmr69420/nix-installer-scripts/main/installer-scripts/regular-nix-installer-selinux.sh | bash
```

check selinux status with sestatus

# Update nix

```
nix-channel --list

nix-channel --update
```

# Create or link applications folder

Programs not showing up in start menu

NIX stores all the .desktop files for the programs it installs @ /home/$USER/.nix-profile/share/applications/ and a simple symlink will fix them not showing up in your start menu.

```
mkdir -p /home/$USER/.local/share/applications/

ln -s /home/$USER/.nix-profile/share/applications/* /home/$USER/.local/share/applications/
```

see Chris Titus nix package manager website - https://christitus.com/nix-package-manager/

.profile should show:

```
export XDG_DATA_DIRS=$HOME/.nix-profiles/share:$HOME/.share:”${XDG_DATA_DIRS:-/usr/local/share/:/usr/share
```

# Install Home Manager

from the offical source: https://nix-community.github.io/home-manager/index.html#sec-install-standalone

Add the appropriate Home Manager channel. If you are following Nixpkgs master or an unstable channel you can run

```
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager

nix-channel --update
```


and if you follow a Nixpkgs version 23.05 channel you can run

nix-channel --add https://github.com/nix-community/home-manager/archive/release-23.05.tar.gz home-manager

nix-channel --update


Run the Home Manager installation command and create the first Home Manager generation:

```
nix-shell '<home-manager>' -A install
```

Updating with home manager:

```
nix-channel --update

home-manager switch
```

Better to log out and back in after the switch for the applications to show up properly in the menu.

# Plasma
auto generate your plasma settings with plasma-manager

```
nix run github:pjones/plasma-manager
```

from https://github.com/pjones/plasma-manager

# Nix-shell

Install a package without installing it globally

```
nix-shell --packages
# or
nix-shell -p
```

for example

```
nix-shell -p hollywood

hollywood
```

# Default configuration location

/home/$USER/.config/home-manager/home.nix

# An easy way to handle Nix packages - DevBox

```
https://www.jetpack.io/devbox/docs/quickstart/
```

## Install Devbox

Needs nix to installed first.

Use the following install script to get the latest version of Devbox:

```
curl -fsSL https://get.jetpack.io/devbox | bash
```

### Using DevBox like nix-shell

Create a new empty directory like devbox or project
cd into that directory
Initialize Devbox:

```
devbox init
```

This creates a devbox.json file in the current directory.
Search for packages to add to the Devbox project with

```
devbox search <packages>
```

Add a package to the project by running:
```
devbox add <package>.
```

To remove a package:
```
devbox rm <package>
```

devbox.json file keeps track of the packages that added.
Start a new shell that has the packages and tools installed:
```
devbox shell
```

This is like the nix-shell
To exit the Devbox shell and return to the regular shell:

exit

see https://www.jetpack.io/devbox/docs/quickstart/

Using DevBox as a primary package manager like home manager
To install global packages:
```
devbox global add <packages>
```

To remove global packages:
```
devbox global rm <packages>
```

To view a full list of global packages, run:
```
devbox global list
```


#### Using Global Packages in the Host Shell
To make the global packages available in the host shell, add them to the shell PATH by running:
```
devbox global shellenv
```

will print the command necessary to source the packages.
see https://www.jetpack.io/devbox/docs/devbox_global/

## Using Fleek with DevBox Global
```
devbox global pull https://devbox.getfleek.dev/high
```

### Install the bash shell hook with devbox global
```
devbox global run install-hook-bash
```

#### View the full list of scripts using devbox global run with no arguments

Global sub commands

```
devbox global add - Add a global package to devbox
devbox global list - List global packages
devbox global pull - Pulls a global config from a file or URL
devbox global rm - Remove a global package
devbox global shellenv - Print shell commands that add global Devbox packages to your PATH
```

see https://www.jetpack.io/devbox/docs/cli_reference/devbox_global/

# Default global path

```
~/.local/share/devbox/global/default
```

or run
```
devbox global path
```







